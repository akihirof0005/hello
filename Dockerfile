FROM archlinux/base
RUN curl https://www.archlinux.org/mirrorlist/\?country\=JP\&protocol\=https\&ip_version\=4\&ip_version\=6 -o /etc/pacman.d/mirrorlist.bak && sed -e "s/^#//g" /etc/pacman.d/mirrorlist.bak >  /etc/pacman.d/mirrorlist
RUN yes "" | pacman -Syy libffi curl maven jq pandoc jdk8-openjdk && rm -rf /var/cache/pacman/pkg

RUN useradd -m -s /bin/bash docker
RUN echo 'Defaults visiblepw'            >> /etc/sudoers
RUN echo 'docker ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers

USER docker
WORKDIR /home/docker

