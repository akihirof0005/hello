# mavenリポジトリ
これは、nexusなどのmavenリポジトリの機能などをgitlabで提供するためのサンプルリポジトリ   
`mvn install`で生成するローカルリポジトリを静的なwebページとして公開している仕組み   
mvn deploy で登録はできないが、pushごとにCIで更新され,[リリース](https://gitlab.com/akihirof0005/hello/-/releases)されている成果物を配信する。   

# 配布バージョン
Latestバージョン   
[リリース](https://gitlab.com/akihirof0005/hello/-/releases)   

# URL

https://akihirof0005.gitlab.io/hello/   

# サンプルPOM

以下のように参照すれば良い。   

```xml
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
  <modelVersion>4.0.0</modelVersion>
      <groupId>org.sample</groupId>
  <artifactId>my-app</artifactId>
  <packaging>jar</packaging>
  <version>1.0.0</version>
  <name>my-app</name>
  <properties>
  <maven.compiler.source>1.8</maven.compiler.source>
  <maven.compiler.target>1.8</maven.compiler.target>
  <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
  </properties>
      <repositories>
        <repository>
           <id>org.sample</id>
           <url>https://akihirof0005.gitlab.io/hello/</url>
           <releases>
              <enabled>true</enabled>
           </releases>
        </repository>
     </repositories>
  <dependencies>
    <dependency>
      <groupId>junit</groupId>
      <artifactId>junit</artifactId>
      <version>3.8.1</version>
      <scope>test</scope>
    </dependency>
    <dependency>
      <groupId>org.sample</groupId>
      <artifactId>hello</artifactId>
      <version>latest</version>
    </dependency>
  </dependencies>
</project>
```
