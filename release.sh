## 
dir=`pwd`
mkdir -p ./public/org

## latest version
sed -i -e 's/<version>.*replace-->$/<version>daily-snapshot<\/version>/' pom.xml
mvn -Dmaven.repo.local=./repo/ clean install
mv ./repo/org/sample ./public/org

## javadoc
mvn javadoc:javadoc
mv ./target/site/apidocs ./public/

## releases version
json=`curl "https://gitlab.com/api/v4/projects/17557723/releases?private_token=$CI_DEPLOY_PASSWORD"`
len=$(echo $json | jq length)
for i in $( seq 0 $(($len - 1)) ); do
  version=$(echo $json | jq -r .[$i].name)
  link=$(echo $json | jq -r .[$i].assets.links)
  len2=$(echo $link | jq length)
  mkdir "$dir/public/org/sample/hello/$version"
  cd "$dir/public/org/sample/hello/$version"
  for k in $( seq 0 $(($len2 - 1)) ); do
    curl $(echo $link | jq -r .[$k].url) -o  $(echo $link | jq -r .[$k].name)
  done
done

## create index.html
cd "$dir/public"
curl "https://gist.githubusercontent.com/andyferra/2554919/raw/10ce87fe71b23216e3075d5648b8b9e56f7758e1/github.css" -o ./github.css
pandoc -s ../README.md -t html5 -c github.css -o ./index.html
echo "<html><body><h1>Directory listing:</h1>" >> ./index.html
find ./org/sample/hello/ -exec echo "<a href='{}'>{}</a><br/>" \; >> ./index.html
echo "</body></html>" >> ./index.html
